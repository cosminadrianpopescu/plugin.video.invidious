import routing
from resources.lib.json_store import JSONStore
from resources.lib.kodiutils import build_url, logger
from xbmcplugin import addDirectoryItem, endOfDirectory
from xbmcgui import ListItem

plugin = routing.Plugin()

def items(type, channel_id = None):
    store_name = type if channel_id is None else 'channel-videos'
    store = JSONStore("%s-search.json" % store_name)
    terms = store.get_terms()
    url = build_url('/search/videos/%s' % type if channel_id is None else '/search/channel/%s' % channel_id)
    addDirectoryItem(plugin.handle, url, ListItem("Search"), True)
    for term in terms:
        url = build_url('/%s/%s/page/1' % (type, term) if channel_id is None else '/channel/%s/search/%s/page/1' % (channel_id, term))
        addDirectoryItem(plugin.handle, url, ListItem(term), True)
    endOfDirectory(plugin.handle)

