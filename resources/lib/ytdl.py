import os
import subprocess
import re
import sys
from resources.lib.kodiutils import logger
from resources.lib.kodiutils import get_setting

def get_urls(id):
    info = getVideoInfo(id, 1)
    return {'title': info.title, 'formatStreams' : [{'url': info.streamURL(), 'qualityLabel': ''}]}
