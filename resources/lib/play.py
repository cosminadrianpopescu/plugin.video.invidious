import routing
import requests
import xbmc
import json
import os
import sys
from subprocess import PIPE, Popen
import xbmcgui
import ytdl
from resources.lib.kodiutils import get_setting, build_url, get_setting_as_bool
from resources.lib.kodiutils import logger
from xbmcplugin import setResolvedUrl
import xbmcaddon

plugin = routing.Plugin()

qualities = ["4K", "1080p", "720p", "480p", "360p", "240p"]

def url(id):
    quality = get_setting("quality")
    base_url = get_setting("server_url")
    return "%s/get/?id=%s&quality=%s" % (base_url, id, quality)

def local_youtube_dl(id):
    addon = xbmcaddon.Addon()
    path = "%s/resources/bin/youtube-dl" % str(addon.getAddonInfo("path"))
    (stdin, stdout) = os.popen2("%s --simulate --dump-json -- %s" % (path, id))
    output = stdout.read()
    return json.loads(output)

def get_url(id):
    r = requests.get(url(id))
    return json.loads(r.content)

def play_youtube_dl(id):
    data = local_youtube_dl(id)
    quality = get_setting("quality")
    formats = filter(lambda x: x['acodec'] != 'none' and x['vcodec'] != 'none', data['formats'])
    logger.log("GOT FORMATS %s" % str(map(lambda x: x['format'], formats)))
    _format = filter(lambda x: x['format_note'] == quality, formats)
    format = _format[0] if len(_format) > 1 else formats[len(formats) - 1]
    logger.log("GOT FORMAT %s" % str(format))
    if (format is not None):
        item = xbmcgui.ListItem(path = format['url'])
        item.setInfo('video', {'title': data['title'], 'plot': data['description']})
        setResolvedUrl(plugin.handle, True, item)
    # content = get_url(id)

def play(id):
    data = get_url(id)
    logger.log("GOT JSON %s" % str(data))
    item = xbmcgui.ListItem(path = data['url'])
    item.setInfo('video', {'title': data['title'], 'plot': data['description']})
    setResolvedUrl(plugin.handle, True, item)
    # content = get_url(id)
