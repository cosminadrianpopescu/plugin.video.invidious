# -*- coding: utf-8 -*-

import routing
import sys
import xbmc
import xbmcaddon
from resources.lib.search import search_video, search_channel, search_video_in_channel, search_playlists, playlists_videos
from resources.lib.play import play
from xbmcgui import ListItem
from xbmcplugin import addDirectoryItem, endOfDirectory
from resources.lib import logger
from resources.lib.kodiutils import get_input
from resources.lib.search_page import items


ADDON = xbmcaddon.Addon()
plugin = routing.Plugin()

# Main page

@plugin.route('/')
def index():
    # addDirectoryItem(plugin.handle, plugin.url_for(
    #     show_category), ListItem("Category One"), True)
    addDirectoryItem(plugin.handle, plugin.url_for(search_route, "video"), ListItem("Videos"), True)
    addDirectoryItem(plugin.handle, plugin.url_for(search_route, "channel"), ListItem("Channels"), True)
    endOfDirectory(plugin.handle)


@plugin.route('/search/<type>')
def search_route(type):
    items(type)

@plugin.route('/channel/videos/<channel_id>/page/<page>')
def channel(channel_id, page):
    search_video(channel_id = channel_id, page = page)

@plugin.route('/channel/<channel>/search')
def search_in_channel_items(channel):
    items('channel', channel)

@plugin.route('/channel/<channel>/playlists')
def search_playlists_videos(channel):
    search_playlists(channel)

@plugin.route('/channel/<channel>/playlists/<cont>')
def search_playlists_videos_cont(channel, cont):
    search_playlists(channel, cont)

@plugin.route('/search/videos/<type>')
def search_videos(type):
    text = get_input(type, 'Query: ')
    if (text is not None):
        (search_channel if type == 'channel' else search_video)(text)

@plugin.route('/search/channel/<channel_id>')
def do_search_channel(channel_id):
    text = get_input('channel-videos', 'Query: ')
    if (text is not None):
        search_video_in_channel(text, channel_id)

@plugin.route('/play/<id>')
def do_play(id):
    play(id)

@plugin.route('/video/<query>/page/<page>')
def next_video_page(query, page):
    search_video(query, page = page)

@plugin.route('/channel/<channel>/search/<query>/page/<page>')
def search_in_channel(channel, query, page):
    search_video_in_channel(query, channel, page)

@plugin.route('/playlists/<which>/videos')
def playlists_videos_route(which):
    playlists_videos(which)

@plugin.route('/channel/<channel>/page/<page>')
def next_channel_page(channel, page):
    search_channel(channel, page)

def run():
    plugin.run()
