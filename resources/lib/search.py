import routing
import requests
import xbmc
import json
import sys
import xbmcgui
from datetime import datetime
from resources.lib.kodiutils import get_setting, build_url, urlencode
from xbmcplugin import addDirectoryItem, endOfDirectory, setContent
from resources.lib.kodiutils import get_input, logger

plugin = routing.Plugin()

def perform_search(url):
    r = requests.get(url)
    return json.loads(r.content)


def do_search(q, which, channel_id = None, playlist_id = None, page = 1):
    url = get_setting("server_url")
    if channel_id is None and playlist_id is None:
        _url = "%s/search/?query=%s&what=%s&page=%s" % (url, q, which, page)
    elif which == "playlists" and playlist_id is None:
        _url = "%s/search/?userId=%s&what=playlist&page=%s" % (url, channel_id, page)
    elif which == 'playlists' and playlist_id is not None:
        _url = "%s/search/?query=%s&what=playlist-videos" % (url, playlist_id)
    elif q == "":
        _url = "%s/search/?userId=%s&page=%s&what=user-videos" % (url, channel_id, page)
    else:
        _url = "%s/search/?query=%s&what=video&userId=%s&page=%s" % (url, q, channel_id, page)

    logger.log("URL IS %s" % _url)

    return perform_search(_url)


def get_thumbnail(v, key1, key2, value2):
    result = None
    if (key1 in v.keys()):
        for t in v[key1]:
            if t[key2] == value2:
                result = t
                break
        if (result is None):
            result = v[key1][len(v[key1]) - 1]

    if result is not None:
        return result['url']

    return None

def _render_videos(data):
    setContent(plugin.handle, "videos")
    for v in data['data']:
        if ('id' not in v.keys()):
            continue
        logger.log("GOT DATA %s" % str(v))
        if v['title'].replace(' ', '') == '':
            continue
        thumb_url = v['thumbnailUrl']
        # fan_art_url = get_thumbnail(v, 'videoThumbnails', 'quality', 'maxres')
        item = xbmcgui.ListItem(label = v['title'], thumbnailImage = thumb_url)
        # if (fan_art_url is not None):
        #     item.setProperty('fanart_image', fan_art_url)
        
        # date = v['date'] if 'date' in v.keys() else ''
        desc = v['description'] if 'description' in v.keys() else ''
        duration = int(v['duration']) if v['duration'] is not None else 0
        item.setInfo('video', {'title': v['title'], 'Duration': int(duration), 'plot': desc})
        item.addStreamInfo('video', {'Duration': int(duration)})
        item.setProperty('isPlayable', 'true')
        url = build_url('/play/%s' % v['id'])
        addDirectoryItem(plugin.handle, url, item, False)

        # item = xbmcgui.ListItem(label = d['title'], thumbnailImage=d[""])
        # addDirectoryItem(plugin.handle, plugin.url_for(search_route), ListItem("Search"), True)


def search_video(query = "", channel_id = None, page = 1):
    d = do_search(query, "video", channel_id = channel_id, page = page)
    if (channel_id is not None):
        addDirectoryItem(plugin.handle, build_url('/channel/%s/search' % channel_id), xbmcgui.ListItem("Search videos in this channel"), True)
        addDirectoryItem(plugin.handle, build_url('/channel/%s/playlists' % channel_id), xbmcgui.ListItem("Playlists"), True)
    
    _render_videos(d)

    _p = str(int(page) + 1)
    url = build_url("/%s/%s/%s" % ('video' if channel_id is None else 'channel/videos', urlencode(query) if channel_id is None else channel_id, 'page/%s' % _p))
    addDirectoryItem(plugin.handle, url, xbmcgui.ListItem("Next page (%s)" % _p), True)
    endOfDirectory(plugin.handle)

def search_video_in_channel(q, channel_id, page = 1):
    d = do_search(q, "channel", channel_id = channel_id, page = page)
    _render_videos(d)

    # _p = str(int(page if page is not None else 1) + 1)
    # addDirectoryItem(plugin.handle, build_url('/channel/%s/page/%s' % (q, _p)), xbmcgui.ListItem("Next page (%s)" % _p), True)
    endOfDirectory(plugin.handle)

def search_channel(q, page = 1, channel_id = None):
    d = do_search(q, "user", channel_id = channel_id, page = page)
    for v in d['data']:
        thumb_url = v['thumbnailUrl']
        item = xbmcgui.ListItem(label = v['name'], thumbnailImage = thumb_url)
        desc = v['description'] if 'description' in v.keys() else ''
        
        item.setInfo('video', {'title': v['name'], 'plot': desc})
        url = build_url('/channel/videos/%s/page/1' % v['id'])
        addDirectoryItem(plugin.handle, url, item, True)

        # item = xbmcgui.ListItem(label = d['title'], thumbnailImage=d[""])
        # addDirectoryItem(plugin.handle, plugin.url_for(search_route), ListItem("Search"), True)

    _p = str(int(page if page is not None else 1) + 1)
    addDirectoryItem(plugin.handle, build_url('/channel/%s/page/%s' % (q, _p)), xbmcgui.ListItem("Next page (%s)" % _p), True)
    endOfDirectory(plugin.handle)

def search_playlists(channel_id, continuation = None):
    d = do_search("", "playlists", channel_id = channel_id, page = continuation)
    for v in d['data']:
        thumb_url = v['thumbnailUrl']
        title = "%s (%s videos)" % (v['title'], v['videos'])
        item = xbmcgui.ListItem(label = title, thumbnailImage = thumb_url)
        
        item.setInfo('video', {'title': v['author'] if 'author' in v.keys() else ''})
        url = build_url('/playlists/%s/videos' % v['id'])
        addDirectoryItem(plugin.handle, url, item, True)

    endOfDirectory(plugin.handle)

def playlists_videos(playlist_id):
    d = do_search("", "playlists", playlist_id = playlist_id)
    _render_videos(d)
    endOfDirectory(plugin.handle)
